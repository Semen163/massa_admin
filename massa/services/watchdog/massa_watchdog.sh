#!/bin/bash

# This script must run with root priviledges to restart massa service

source /usr/local/lib/prettylogs.sh
client="/usr/local/bin/massa_client"
unpriviledged_user=daniel
declare -i min_lines=43
main() {
    echo -e $(get_trace "INFO" "Starting massa node's watchdog")
    local output
    # drop root priviledges and call client
    output=$(su "$unpriviledged_user" -c "timeout 10 $client get_status | wc -l")
    if (( output < min_lines)); then
        unset output
        echo -e $(get_trace "ERROR" "Massa node's crash detected")
        echo -e $(get_trace "INFO" "Restarting massa node")
        systemctl restart massa
        # wait 300s for the node to bootstrap
        sleep 300
        # drop root priviledges and call client
        output=$(su "$unpriviledged_user" -c "timeout 10 $client get_status | wc -l")
        if (( output < min_lines)); then
            echo -e $(get_trace "ERROR" "Failed to restart massa node")
        else
            echo -e $(get_trace "INFO" "Massa node successfully restarted")
        fi
    else
        echo -e $(get_trace "INFO" "So far so good")
    fi
    echo -e $(get_trace "INFO" "Terminated")
}

main
